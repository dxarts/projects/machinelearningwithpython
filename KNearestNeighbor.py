import sys
import pandas as pd
import matplotlib
import numpy as np
import scipy as sp
import sklearn
import matplotlib.pyplot as plt
import mglearn
from sklearn.model_selection import train_test_split

from sklearn.datasets import load_iris
# load data set
iris_dataset = load_iris()

# get the keys of the data set
print("Keys of iris_dataset: \n{}".format(iris_dataset.keys()))
# print description of data
print(iris_dataset['DESCR'] + "\n...")

# split data into training set and test select
# X -> raw data
# Y -> classification of corresponding data in X
X_train, X_test, y_train, y_test = train_test_split(iris_dataset['data'], iris_dataset['target'], random_state=0)

# create scatter matrix plot using pandas (needs pandas DataFrame)
# create dataframe from data in X_train
# label the columns using the strings in iris_dataset.feature_names
iris_dataframe = pd.DataFrame(X_train, columns=iris_dataset.feature_names)

# create a scatter matrix from the dataframe, color by y_train
from pandas.plotting import scatter_matrix
grr = scatter_matrix(iris_dataframe, c=y_train, figsize=(15, 15), marker='o', hist_kwds={'bins': 20}, s=60, alpha=.8, cmap=mglearn.cm3)

# create a k-neighbors classifier
from sklearn.neighbors import KNeighborsClassifier
knn = KNeighborsClassifier(n_neighbors=1)

# supply the data to the classifier
knn.fit(X_train, y_train)

# create random sample data to test classifier
X_new = np.array([[5, 2.9, 1, 0.2]])

# test the sample data
prediction = knn.predict(X_new)
print("Prediction: {}".format(prediction))
print("Predicted target name: {}".format(iris_dataset['target_names'][prediction]))

# use the test data from above
y_pred = knn.predict(X_test)
print("Test set predictions:\n {}".format(y_pred))

# check with test results
print("Test set score: {:.2f}".format(np.mean(y_pred == y_test)))

# generate dataset
X, y = mglearn.datasets.make_forge()
# plot dataset
mglearn.discrete_scatter(X[:, 0], X[:, 1], y)
plt.legend(["Class 0", "Class 1"], loc=4)
plt.xlabel("First feature")
plt.ylabel("Second feature")

print("X.shape: {}".format(X.shape))

X, y = mglearn.datasets.make_wave(n_samples=40)
plt.ylim(-3, 3)
plt.xlabel("Feature")
plt.ylabel("Target")
plt.plot(X, y, 'o')

from sklearn.datasets import load_breast_cancer
cancer = load_breast_cancer()
print("cancer.keys(): \n{}".format(cancer.keys()))

mglearn.plots.plot_knn_classification(n_neighbors=1)

from sklearn.model_selection import train_test_split
X, y = mglearn.datasets.make_forge()
X_train, X_test, y_train, y_test = train_test_split(X, y, random_state=0)

from sklearn.neighbors import KNeighborsClassifier
clf = KNeighborsClassifier(n_neighbors=3)

clf.fit(X_train, y_train)
print("Test set predictions: {}".format(clf.predict(X_test)))
